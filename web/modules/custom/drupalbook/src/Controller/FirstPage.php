<?php

namespace Drupal\drupalbook\Controller;

class FirstPage {
  public function content(){
    $output = node_load_multiple();
    $output = node_view_multiple($output);
    return array(
      '#markup' => render($output),
    );
  }
}
